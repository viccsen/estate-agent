import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import auth from './auth'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    router,
    auth,
  }
})

export default store
